import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter_babyhealth/Page/Menu.dart';

class KontenOne extends StatefulWidget {
  @override
  _KontenOneState createState() => _KontenOneState();
}
class _KontenOneState extends State<KontenOne> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: _PageList(),
        backgroundColor: Colors.black87,
      ),
    );
  }
}

class _PageList extends StatefulWidget{
  @override
  _PageListState createState() => _PageListState();
}
class _PageListState extends State<_PageList> {
  List names = [
    "LAPORAN",
    "INFORMASI",
    "PENGATURAN",
    "BANTUAN",
    "LOGOUT",


  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: ListView.builder(
        itemBuilder:(BuildContext context, int index){
          final number = index+1;
          final name = names[index].toString();
          return Card(
            color: Colors.grey,
            child:Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CircleAvatar(
                    backgroundImage: AssetImage('lib/assets/logo.jpg'),
                  ),
                  Text(name),
                  Icon(Icons.check),
                ],
              ),
            ),
          );
        },
        itemCount: names.length,
      ),
    );
  }
}

