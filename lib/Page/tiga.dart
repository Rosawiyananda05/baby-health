import 'package:flutter/material.dart';


class Kontentiga extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Baby Health ',
      home: MyHomePage(),
    );
  }
}
class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState()=> _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage>with SingleTickerProviderStateMixin{
  AnimationController _animationController;
  bool isPlaying =false;
  Color Kotak = Colors.brown;

  @override
  void initState(){
    super.initState();
    _animationController= AnimationController(vsync: this,duration: Duration(milliseconds: 450));
  }
  @override
  void dispose() {
    super.dispose();
    _animationController.dispose();
  }
  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[ Container(width: 400, height: 500, color: Kotak,
            child: Center(
              child: IconButton(
                  iconSize: 250,
                  icon: AnimatedIcon(
                    icon: AnimatedIcons.play_pause,
                    progress: _animationController,
                    color: Colors.white,
                  ),
                  onPressed: () => _handleOnPressed()
              ),
            ),
          ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment:
              MainAxisAlignment.center,
              children:<Widget>[
                MaterialButton(

                  onPressed: (){
                    setState((){Kotak = Colors.white;});
                    _handleOnPressed();
                  },
                  color: Colors.white,),
                MaterialButton(

                  onPressed: (){
                    setState((){Kotak = Colors.white;});
                    _handleOnPressed();
                  },
                  color: Colors.white,),
                MaterialButton(

                  onPressed: (){
                    setState((){Kotak = Colors.white;});
                    _handleOnPressed();
                  },
                  color: Colors.white,),
                MaterialButton(

                  onPressed: (){
                    setState((){Kotak = Colors.white;});
                    _handleOnPressed();
                  },
                  color: Colors.white,),
              ],
            ),
          ],
        ),
      ),
      backgroundColor: Colors.white,
    );
  }
  void _handleOnPressed(){
    setState(() {
      isPlaying = !isPlaying;
      isPlaying
          ?_animationController.forward():_animationController.reverse();
    });
  }

}