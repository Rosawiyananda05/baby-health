import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter_babyhealth/Page/Login.dart';
class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final alucard = Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.all(20),
        child: CircleAvatar(
          radius: 200,
          backgroundColor: Colors.transparent,
          backgroundImage: AssetImage('lib/assets/logo.jpg'),
        ),
      ),
    );

    final welcome = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Baby Health ',
        style: TextStyle(fontSize: 40.0, color: Colors.black),
      ),
    );

    final lorem = Padding(
      padding: EdgeInsets.all(20.0),
      child: RichText(
        text: TextSpan(
            children: [
              TextSpan(
                text: ' Selamat Datang\n',
                style: TextStyle(fontSize: 20.0, color: Colors.black),
              ),
              TextSpan(
                  text: '\n \n \n Klik disini Untuk Masuk',
                  style: TextStyle(fontSize: 15.0, color: Colors.blueAccent,fontFamily: 'Canterbury'),
                  recognizer: TapGestureRecognizer()..
                  onTap =() {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginPage()),
                    );
                  }
              ),
            ]
        ),
      ),
    );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(30.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.white60,
          Colors.white60,
        ]),
      ),
      child: Column(
        children: <Widget>[alucard, welcome, lorem],
      ),
    );
    return Scaffold(
      body:body,
    );
  }
}
