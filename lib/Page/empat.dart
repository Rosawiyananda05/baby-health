import 'package:flutter/material.dart';

class KontenEmpat extends StatefulWidget{
  @override
  _KontenEmpatState createState() => _KontenEmpatState();
}
class _KontenEmpatState extends State<KontenEmpat>{
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        elevation: 1.5,
        backgroundColor: Colors.white,
        title: new Center(
          child: new Text(
            "Baby Health",
            style: new TextStyle(color: Colors.black),
          ),
        ),
        leading: new IconButton(
          icon: new Icon(
            Icons.camera_alt,
            color: Colors.black,
          ),
          onPressed: () {},
        ),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(
              Icons.message,
              color: Colors.black,
            ),
            onPressed: () {},
          )
        ],
      ),
      body: new SingleChildScrollView(
        child: new Column(
          children: <Widget>[new PostItem(), new PostItem2(),new PostItem3(),new PostItem4()],
        ),
      ),
      backgroundColor: Colors.white,
    );
  }
}

class PostItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        _buildUserprofile(),
        _buildUserPost(),
        _buildActionButtons(),
      ],
    );
  }

  Widget _buildUserprofile() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Row(
        children: <Widget>[
          new CircleAvatar(
            backgroundImage: AssetImage('lib/assets/logo.jpg'),
            radius: 24.0,
          ),
          new Expanded(
            child: new Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: new Text("Baby Health",style: TextStyle( color:Colors.black)),
            ),
          ),
          new IconButton(
            icon: new Icon(
              Icons.more_vert,
              color: Colors. black,
            ),
            onPressed: () {},
          )
        ],
      ),
    );
  }

  Widget _buildUserPost() {
    return new Container(
        child:Image(
          image: AssetImage("lib/assets/logo.jpg"),
        )
    );
  }

  Widget _buildActionButtons() {
    return new Container(
      child: new Row(
        children: <Widget>[
          new Expanded(
              child: new Row(
                children: <Widget>[
                  new IconButton(
                    icon: new Icon(
                      Icons.mode_comment_outlined,
                      color: Colors.white,
                    ),
                    onPressed: () {},
                  ),
                  new IconButton(
                    icon: new Icon(Icons.favorite_outline, color: Colors.white,),
                    onPressed: () {},
                  ),
                ],
              )),
          new IconButton(
            icon: new Icon(
              Icons.bookmark_outline,
              color: Colors.black,
            ),
            onPressed: () {},
          )
        ],
      ),
    );
  }
}
class PostItem2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        _buildUserprofile(),
        _buildUserPost(),
        _buildActionButtons(),
      ],
    );
  }

  Widget _buildUserprofile() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Row(
        children: <Widget>[
          new CircleAvatar(
            backgroundImage: AssetImage('lib/assets/logo.jpg'),
            radius: 24.0,
          ),
          new Expanded(
            child: new Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: new Text("Baby Health",style: TextStyle( color:Colors.black)),
            ),
          ),
          new IconButton(
            icon: new Icon(
              Icons.more_vert,
              color: Colors. black,
            ),
            onPressed: () {},
          )
        ],
      ),
    );
  }

  Widget _buildUserPost() {
    return new Container(
        child:Image(
          image: AssetImage("lib/assets/1.jpg"),
        )
    );
  }

  Widget _buildActionButtons() {
    return new Container(
      child: new Row(
        children: <Widget>[
          new Expanded(
              child: new Row(
                children: <Widget>[
                  new IconButton(
                    icon: new Icon(
                      Icons.mode_comment_outlined,
                      color: Colors.black,
                    ),
                    onPressed: () {},
                  ),
                  new IconButton(
                    icon: new Icon(Icons.favorite_outline, color: Colors.white,),
                    onPressed: () {},
                  ),
                ],
              )),
          new IconButton(
            icon: new Icon(
              Icons.bookmark_outline,
              color: Colors.black,
            ),
            onPressed: () {},
          )
        ],
      ),
    );
  }
}
class PostItem3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        _buildUserprofile(),
        _buildUserPost(),
        _buildActionButtons(),
      ],
    );
  }

  Widget _buildUserprofile() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Row(
        children: <Widget>[
          new CircleAvatar(
            backgroundImage: AssetImage('lib/assets/logo.jpg'),
            radius: 24.0,
          ),
          new Expanded(
            child: new Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: new Text("Baby Health",style: TextStyle( color:Colors.black)),
            ),
          ),
          new IconButton(
            icon: new Icon(
              Icons.more_vert,
              color: Colors. black,
            ),
            onPressed: () {},
          )
        ],
      ),
    );
  }

  Widget _buildUserPost() {
    return new Container(
        child:Image(
          image: AssetImage("lib/assets/2.jpg"),
        )
    );
  }

  Widget _buildActionButtons() {
    return new Container(
      child: new Row(
        children: <Widget>[
          new Expanded(
              child: new Row(
                children: <Widget>[
                  new IconButton(
                    icon: new Icon(
                      Icons.mode_comment_outlined,
                      color: Colors.black,
                    ),
                    onPressed: () {},
                  ),
                  new IconButton(
                    icon: new Icon(Icons.favorite_outline, color: Colors.white,),
                    onPressed: () {},
                  ),
                ],
              )),
          new IconButton(
            icon: new Icon(
              Icons.bookmark_outline,
              color: Colors.black,
            ),
            onPressed: () {},
          )
        ],
      ),
    );
  }
}
class PostItem4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        _buildUserprofile(),
        _buildUserPost(),
        _buildActionButtons(),
      ],
    );
  }

  Widget _buildUserprofile() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Row(
        children: <Widget>[
          new CircleAvatar(
            backgroundImage: AssetImage('lib/assets/logo.jpg'),
            radius: 24.0,
          ),
          new Expanded(
            child: new Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: new Text("Baby Health",style: TextStyle( color:Colors.black)),
            ),
          ),
          new IconButton(
            icon: new Icon(
              Icons.more_vert,
              color: Colors. black,
            ),
            onPressed: () {
            },
          )
        ],
      ),
    );
  }

  Widget _buildUserPost() {
    return new Container(
        child:Image(
          image: AssetImage("lib/assets/3.jpg"),
        )
    );
  }

  Widget _buildActionButtons() {
    return new Container(
      child: new Row(
        children: <Widget>[
          new Expanded(
              child: new Row(
                children: <Widget>[
                  new IconButton(
                    icon: new Icon(
                      Icons.mode_comment_outlined,
                      color: Colors.black,
                    ),
                    onPressed: () {},
                  ),
                  new IconButton(
                    icon: new Icon(Icons.favorite_outline, color: Colors.white,),
                    onPressed: () {},
                  ),
                ],
              )),
          new IconButton(
            icon: new Icon(
              Icons.bookmark_outline,
              color: Colors.black,
            ),
            onPressed: () {},
          )
        ],
      ),
    );
  }
}