
import 'package:flutter/material.dart';
import 'package:flutter_babyhealth/Page/satu.dart';
import 'package:flutter_babyhealth/Page/dua.dart';
import 'package:flutter_babyhealth/Page/tiga.dart';
import 'package:flutter_babyhealth/Page/empat.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BjrNavBar(),
    );
  }
}

class BjrNavBar extends StatefulWidget {
  @override
  _BjrNavBarState createState() => _BjrNavBarState();
}

class _BjrNavBarState extends State<BjrNavBar>with SingleTickerProviderStateMixin {
  //int currentIndex = 0;
  int selectedNavBar = 0;
  TabController controller;
  @override
  void initState() {
    controller = new TabController(length: 4, vsync: this);
    super.initState();
  }
  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
  void _changeSelectedNavBar(int index) {
    setState(() {
      //currentIndex = index;
      selectedNavBar = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Center(
          child :Text("MENU"),
        ),
        backgroundColor: Colors.blue,
      ),
      body: new TabBarView(
        controller: controller,
        children: <Widget>[
          new KontenEmpat(),
          new KontenOne(),
          new Kontentiga(),
          new KontenDua(),

        ],
      ),
      bottomNavigationBar:new Material(
          color : Colors.blue,
          child : new TabBar(
              controller : controller,
              tabs: <Widget>[
                new Tab(icon: new Icon(Icons.home,color: Colors.white),text: 'HOME',),
                new Tab(icon: new Icon(Icons.language_outlined,color: Colors.white),text: 'DAFTAR',),
                new Tab(icon: new Icon(Icons.filter_tilt_shift,color: Colors.white),text: 'ACTIVITY',),
                new Tab(icon: new Icon(Icons.person,color: Colors.white),text: 'AKUN',),
              ]
          )
      ),
    );

  }
}