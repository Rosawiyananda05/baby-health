import 'package:flutter/material.dart';


class KontenDua extends StatefulWidget {
  @override
  _KontenDuaState createState() => _KontenDuaState();
}

class _KontenDuaState extends State<KontenDua> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircleAvatar(
                  radius: 100,
                  backgroundImage: AssetImage('lib/assets/3.jpg'),
                ),
                SizedBox(
                  height: 20,
                  width: 300,
                  child: Divider(
                    color: Colors.black,
                  ),
                ),
                Text(
                  'Baby Health',
                  style: TextStyle(
                    fontSize: 25,
                    color: Colors.black,
                  ),
                ),
                Text(
                  'Angel',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    letterSpacing: 3.5,
                  ),
                ),
                SizedBox(
                  height: 20.0,
                  width: 200,
                  child: Divider(
                    color: Colors.teal[100],
                  ),
                ), Text("INFORMASI PENGGUNA",style: TextStyle(color: Colors.black),),
                Card(
                    color: Colors.grey,
                    margin:
                    EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                    child: ListTile(
                      leading: Icon(
                        Icons.phone,
                        color: Colors.green,
                      ),
                      title: Text(
                        '+62 8XX6893XXX78',
                        style:
                        TextStyle(fontFamily: 'BalooBhai', fontSize: 20.0,color: Colors.white),
                      ),
                    )),
                Card(
                  color: Colors.grey,
                  margin:
                  EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
                  child: ListTile(
                    leading: Icon(
                      Icons.cake,
                      color: Colors.green,
                    ),
                    title: Text(
                      '1-07-2021',
                      style: TextStyle(fontSize: 20.0, color: Colors.white),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}